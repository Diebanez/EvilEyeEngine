#ifndef TRANSFORM_H
#define TRANSFORM_H
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/common.hpp>
class Transform {
public:
	Transform();
	Transform(Transform* parent);
	Transform(glm::vec3 position, glm::quat rotation, glm::vec3 scale);
	Transform(glm::vec3 position, glm::quat rotation, glm::vec3 scale, Transform* parent);

	glm::vec3 getPosition();
	glm::quat getRotation();
	glm::vec3 getScale();
	glm::mat4 getModelMatrix();

	GLvoid translate(glm::vec3 force);
	GLvoid rotate(glm::quat rotation);
	GLvoid setScale(glm::vec3 scale);
private:
	glm::vec3 mPosition;
	glm::quat mRotation;
	glm::vec3 mScale;

	Transform* mParent;
};
#endif