#include "Transform.h"

Transform::Transform() {
	mPosition = glm::vec3(0, 0, 0);
	mRotation = glm::quat(1, 0, 0, 0);
	mScale = glm::vec3(1, 1, 1);

	mParent = 0;
}

Transform::Transform(Transform* parent) {
	mPosition = glm::vec3(0, 0, 0);
	mRotation = glm::quat(1, 0, 0, 0);
	mScale = glm::vec3(1, 1, 1);

	mParent = parent;
}

Transform::Transform(glm::vec3 position, glm::quat rotation, glm::vec3 scale) {
	mPosition = position;
	mRotation = rotation;
	mScale = scale;

	mParent = 0;
}

Transform::Transform(glm::vec3 position, glm::quat rotation, glm::vec3 scale, Transform* parent) {
	mPosition = position;
	mRotation = rotation;
	mScale = scale;

	mParent = parent;
}

glm::vec3 Transform::getPosition() {
	return mPosition;
}

glm::quat Transform::getRotation() {
	return mRotation;
}

glm::vec3 Transform::getScale() {
	return mScale;
}

glm::mat4 Transform::getModelMatrix() {
	if (mParent == 0) {
		return glm::translate(glm::mat4(1.0), mPosition) * glm::toMat4(mRotation) * glm::scale(glm::mat4(1.0), mScale);
	}
	else {
		return mParent->getModelMatrix() * glm::translate(glm::mat4(1.0), mPosition) * glm::toMat4(mRotation) * glm::scale(glm::mat4(1.0), mScale);
	}
}

GLvoid Transform::translate(glm::vec3 force) {
	mPosition += force;
}

GLvoid Transform::rotate(glm::quat rotation) {
	mRotation *= rotation;
}

GLvoid Transform::setScale(glm::vec3 scale) {
	mScale = scale;
}