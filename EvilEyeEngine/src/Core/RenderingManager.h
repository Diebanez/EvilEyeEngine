#ifndef RENDERINGMANAGER_H
#define RENDERINGMANAGER_H
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <queue>

struct MeshToRender {
	GLint Mesh;
	GLint Shader;
	GLint Texture;
	glm::mat4 ModelMat;
};

class RenderingManager {
public:
	GLFWwindow* rWindow;

	static RenderingManager* get();
	GLboolean initialize();
	GLboolean dispose();
	GLvoid renderFrame();
	GLvoid pipelinePush(GLint mesh, GLint shader, GLint texture, glm::mat4 modelMat);
private:
	static RenderingManager* mInstance;

	const GLchar* rAppTitle = "EvilEyeEngine";
	GLint rWidth = 1280;
	GLint rHeight = 720;
	GLboolean rFullscreen = false;
	GLboolean mInitialized;
	std::queue<MeshToRender*> mMeshToRender;

	RenderingManager();
	GLvoid showFPS();

};


#endif //EVILEYEENGINE_RENDERINGMANAGER_H