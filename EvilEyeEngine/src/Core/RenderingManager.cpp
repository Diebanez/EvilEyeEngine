#include "RenderingManager.h"
#include "../Resources/Shader/ShaderManager.h"
#include "../Resources/Mesh/MeshManager.h"
#include "../Resources/Texture/TextureManager.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <sstream>
RenderingManager* RenderingManager::mInstance = 0;

RenderingManager::RenderingManager() {
	mInitialized = GL_FALSE;
}

RenderingManager* RenderingManager::get() {
	if (mInstance == 0) {
		mInstance = new RenderingManager();
	}
	return mInstance;
}

GLboolean RenderingManager::initialize() {
	if (!mInitialized) {
		if (!glfwInit()) {
			std::cerr << "GLFW initialization failed" << std::endl;
			return GL_FALSE;
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		if (rFullscreen) {
			GLFWmonitor* pMonitor = glfwGetPrimaryMonitor();
			const GLFWvidmode* pVmode = glfwGetVideoMode(pMonitor);

			rWindow = glfwCreateWindow(pVmode->width, pVmode->height, rAppTitle, pMonitor, NULL);
			rWidth = pVmode->width;
			rHeight = pVmode->height;
		}
		else {
			rWindow = glfwCreateWindow(rWidth, rHeight, rAppTitle, NULL, NULL);
		}

		if (rWindow == NULL) {
			std::cerr << "Failed to create GLFW window" << std::endl;
			glfwTerminate();
			return GL_FALSE;
		}

		glfwMakeContextCurrent(rWindow);

		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK) {
			std::cerr << "Failed to initialize GLEW" << std::endl;
			glfwTerminate();
			return GL_FALSE;
		}

		glClearColor(0.23f, 0.38f, 0.47f, 1.0f);
		glViewport(0, 0, rWidth, rHeight);
		glEnable(GL_DEPTH_TEST);

		mInitialized = GL_TRUE;
	}
	return GL_TRUE;
}

GLboolean RenderingManager::dispose() {
	if (mInitialized) {
		glfwTerminate();
		mInitialized = GL_FALSE;
	}
	return GL_TRUE;
}

GLvoid RenderingManager::renderFrame() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	showFPS();

	glm::mat4 view = glm::lookAt(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1), glm::vec3(0, 1, 0));
	glm::mat4 projection = glm::perspective(glm::radians(70.0f), (float)rWidth / (float)rHeight, 0.1f, 200.0f);

	while (!mMeshToRender.empty()) {
		MeshToRender* actualMeshData = mMeshToRender.front();

		Shader* actualShader = ShaderManager::get()->getAt(actualMeshData->Shader);

		actualShader->use();
		
		actualShader->setUniform("view", view);
		actualShader->setUniform("projection", projection);
		actualShader->setUniform("model", actualMeshData->ModelMat);

		//Mesh mesh = *MeshManager::get()->getAt(actualMeshData->Mesh);

		//mesh.draw();

		TextureManager::get()->bindTexture(actualMeshData->Texture);

		MeshManager::get()->drawMesh(actualMeshData->Mesh);

		TextureManager::get()->unbindTexture(actualMeshData->Texture);

		mMeshToRender.pop();
	}

	glfwSwapBuffers(rWindow);
}

GLvoid RenderingManager::pipelinePush(GLint mesh, GLint shader, GLint texture, glm::mat4 modelMat) {
	MeshToRender* meshToRender = new MeshToRender();
	meshToRender->Mesh = mesh;
	meshToRender->Shader = shader;
	meshToRender->Texture = texture;
	meshToRender->ModelMat = modelMat;

	mMeshToRender.push(meshToRender);
}

GLvoid RenderingManager::showFPS() {
	static GLdouble previousSeconds = 0.0;
	static GLint frameCount = 0;
	GLdouble elapsedSeconds;
	GLdouble currentSeconds = glfwGetTime();

	elapsedSeconds = currentSeconds - previousSeconds;

	if (elapsedSeconds > 0.25)
	{
		previousSeconds = currentSeconds;
		GLdouble fps = (GLdouble)frameCount / elapsedSeconds;
		GLdouble msPerFrame = 1000.0 / fps;

		std::ostringstream outs;
		outs.precision(3);
		outs << std::fixed
			<< rAppTitle << "    "
			<< "FPS: " << fps << "    "
			<< "Frame Time: " << msPerFrame << " (ms)";
		glfwSetWindowTitle(rWindow, outs.str().c_str());

		frameCount = 0;
	}

	frameCount++;
}