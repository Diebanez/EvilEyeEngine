#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H
#include "Shader.h"
#include <vector>

class ShaderManager {
public:
	static ShaderManager* get();
	GLboolean initialize();
	GLboolean dispose();
	Shader* getAt(GLint id);
	GLint loadShader(std::string vertexShaderPath, std::string fragmentShaderPath);
	GLint loadShader(Shader* shaderToLoad);

private:
	static ShaderManager* mInstance;
	GLboolean mInitialized;
	std::vector<Shader> mLoadedShader;

	ShaderManager();
};


#endif //SHADERMANAGER_H