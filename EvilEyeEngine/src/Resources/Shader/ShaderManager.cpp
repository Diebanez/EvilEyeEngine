#include "ShaderManager.h"
#include <iostream>
ShaderManager* ShaderManager::mInstance = 0;

ShaderManager::ShaderManager() {
	mInitialized = GL_FALSE;
}

ShaderManager* ShaderManager::get() {
	if (mInstance == 0) {
		mInstance = new ShaderManager();
	}
	return mInstance;
}

GLboolean ShaderManager::initialize() {
	if (!mInitialized) {
		mInitialized = GL_TRUE;
	}
	return GL_TRUE;
}

GLboolean ShaderManager::dispose() {
	if (mInitialized) {
		mInitialized = GL_FALSE;
	}
	return GL_TRUE;
}

Shader* ShaderManager::getAt(GLint id) {
	if (id >= 0 && id < mLoadedShader.size()) {
		return &mLoadedShader[id];
	}
	return NULL;
}

GLint ShaderManager::loadShader(std::string vertexShaderPath, std::string fragmentShaderPath) {
	mLoadedShader.push_back(Shader());
	if (mLoadedShader[mLoadedShader.size() - 1].loadShaders(vertexShaderPath.c_str(), fragmentShaderPath.c_str()) == GL_TRUE) {
		return mLoadedShader.size() - 1;
	} else {
		mLoadedShader.pop_back();
		return -1;
	}
}

GLint ShaderManager::loadShader(Shader* shaderToLoad) {
	mLoadedShader.push_back(*shaderToLoad);
	return  mLoadedShader.size() - 1;
}