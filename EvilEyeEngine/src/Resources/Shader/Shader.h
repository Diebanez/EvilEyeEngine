#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <map>
#include "GL/glew.h"
#include "glm/glm.hpp"
using std::string;

class Shader
{
public:
	Shader();
	~Shader();

	enum ShaderType
	{
		VERTEX,
		FRAGMENT,
		PROGRAM
	};

	GLboolean loadShaders(const GLchar* vsFilename, const GLchar* fsFilename);
	GLvoid use();

	GLuint getProgram() const;

	GLvoid setUniform(const GLchar* name, const glm::vec2& v);
	GLvoid setUniform(const GLchar* name, const glm::vec3& v);
	GLvoid setUniform(const GLchar* name, const glm::vec4& v);
	GLvoid setUniform(const GLchar* name, const glm::mat4& m);

	GLint getUniformLocation(const GLchar * name);

private:
	string fileToString(const string& filename);
	GLvoid  checkCompileErrors(GLuint shader, ShaderType type);
	
	GLuint mHandle;
	std::map<string, GLint> mUniformLocations;
};
#endif // SHADER_H