#include "TextureManager.h"
#include <iostream>

TextureManager* TextureManager::mInstance = 0;

TextureManager::TextureManager() {
	mInitialized = GL_FALSE;
}

TextureManager *TextureManager::get() {
	if (mInstance == 0) {
		mInstance = new TextureManager();
	}
	return mInstance;
}

GLboolean TextureManager::initialize() {
	if (!mInitialized) {
		mInitialized = GL_TRUE;
	}
	return GL_TRUE;
}

GLboolean TextureManager::dispose() {
	if (mInitialized) {
		mInitialized = GL_FALSE;
	}
	return GL_TRUE;
}

Texture2D* TextureManager::getAt(GLint position) {
	if (position >= 0 && position < mLoadedTextures.size()) {
		return mLoadedTextures[position];
	}
	return  NULL;
}

GLint TextureManager::loadTexture(std::string texturePath) {
	mLoadedTextures.push_back(new Texture2D(texturePath));
	return mLoadedTextures.size() - 1;
}

GLvoid TextureManager::bindTexture(GLint position) {
	mLoadedTextures[position]->bind(0);
}

GLvoid TextureManager::unbindTexture(GLint position) {
	mLoadedTextures[position]->unbind(0);
}