#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include "Texture2D.h"
#include <vector>
#include <map>

class TextureManager {
public:
	static TextureManager* get();
	GLboolean initialize();
	GLboolean dispose();
	Texture2D* getAt(GLint position);
	GLint loadTexture(std::string texturePath);
	GLvoid bindTexture(GLint position);
	GLvoid unbindTexture(GLint position);
private:
	static int actualId;
	static TextureManager* mInstance;
	GLboolean mInitialized;
	std::vector<Texture2D*> mLoadedTextures;
	TextureManager();
};


#endif //TEXTUREMANAGER_H