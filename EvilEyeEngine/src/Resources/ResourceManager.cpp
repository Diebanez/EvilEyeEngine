#include "ResourceManager.h"
#include "Mesh/MeshManager.h"
#include "Shader/ShaderManager.h"
#include "Texture/TextureManager.h"
ResourceManager* ResourceManager::mInstance = 0;

ResourceManager::ResourceManager() {
	mInitialized = false;
}
ResourceManager* ResourceManager::get() {
	if (mInstance == 0) {
		mInstance = new ResourceManager();
	}
	return mInstance;
}

GLboolean ResourceManager::initialize() {
	if (!mInitialized) {
		if (!MeshManager::get()->initialize()) {
			return false;
		}
		if (!ShaderManager::get()->initialize()) {
			return false;
		}
		mInitialized = true;
	}
	return true;
}

GLboolean ResourceManager::dispose() {
	if (mInitialized) {
		if (!ShaderManager::get()->dispose()) {
			return false;
		}
		if (!MeshManager::get()->dispose()) {
			return false;
		}
		mInitialized = false;
	}
	return true;
}

GLint ResourceManager::getModelId(std::string modelPath) {
	return -1;
}

GLint ResourceManager::getShaderId(std::string vertexShaderPath, std::string fragmentShaderPath) {
	std::map<std::string, int>::iterator it = mShadersIndices.find(vertexShaderPath + fragmentShaderPath);
	if (it == mShadersIndices.end()) {
		int newShader = ShaderManager::get()->loadShader(vertexShaderPath, fragmentShaderPath);
		if (newShader >= 0) {
			mShadersIndices[vertexShaderPath + fragmentShaderPath] = newShader;
		}
		return newShader;
	}
	return mShadersIndices[vertexShaderPath + fragmentShaderPath];
}

GLint ResourceManager::getTextureId(std::string texturePath) {
	std::map<std::string, int>::iterator it = mTextureIndices.find(texturePath);
	if (it == mTextureIndices.end()) {
		int newTexture = TextureManager::get()->loadTexture(texturePath);
		if (newTexture >= 0) {
			mTextureIndices[texturePath] = newTexture;
		}
		return newTexture;
	}
	return mTextureIndices[texturePath];
}