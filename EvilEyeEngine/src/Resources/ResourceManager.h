#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include "Mesh/Mesh.h"
#include "Shader/Shader.h"
#include<map>

class ResourceManager {
public:
	static ResourceManager* get();
	GLboolean initialize();
	GLboolean dispose();
	GLint getModelId(std::string modelPath);
	GLint getShaderId(std::string vertexShaderPath, std::string fragmentShaderPath);
	GLint getTextureId(std::string texturePath);
private:
	static ResourceManager* mInstance;
	GLboolean mInitialized;
	std::map<std::string, GLint> mShadersIndices;
	std::map<std::string, GLint> mModelIndices;
	std::map<std::string, GLint> mTextureIndices;

	ResourceManager();
};

#endif //RESOURCEMANAGER_H
