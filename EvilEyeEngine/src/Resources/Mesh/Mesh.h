#ifndef MESH_H
#define MESH_H
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include <string>

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;

	Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 texCoords) {
		Position = position;
		Normal = normal;
		TexCoords = texCoords;
	}
};

class Mesh {
public:	
	std::vector<Vertex> mVertices;
	std::vector<GLuint> mIndices;

	Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices);
	~Mesh();

	GLvoid draw();
private:
	
	GLboolean mLoaded;
	GLuint  mVBO, mVAO, mEBO;

	GLvoid initBuffers();
};
#endif //MESH_H