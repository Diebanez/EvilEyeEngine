#ifndef MESHMANAGER_H
#define MESHMANAGER_H

#include "Mesh.h"
#include <vector>
#include <map>

class MeshManager {
public:
	static MeshManager* get();
	GLboolean initialize();
	GLboolean dispose();
	Mesh* getAt(GLint position);
	GLint loadMesh(Mesh* meshToLoad);
	GLvoid drawMesh(GLint meshInt);
private:
	static int actualId;
	static MeshManager* mInstance;
	GLboolean mInitialized;
	std::vector<Mesh*> mLoadedMesh;
	//std::map<int, Mesh> mLoadedMesh;
	MeshManager();
};


#endif //MESHMANAGER_H