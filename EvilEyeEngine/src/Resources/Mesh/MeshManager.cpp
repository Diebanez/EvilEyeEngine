#include "MeshManager.h"
#include <iostream>

MeshManager* MeshManager::mInstance = 0;

MeshManager::MeshManager() {
	mInitialized = GL_FALSE;
}

MeshManager *MeshManager::get() {
	if (mInstance == 0) {
		mInstance = new MeshManager();
	}
	return mInstance;
}

GLboolean MeshManager::initialize() {
	if (!mInitialized) {
		mInitialized = GL_TRUE;
	}
	return GL_TRUE;
}

GLboolean MeshManager::dispose() {
	if (mInitialized) {
		mInitialized = GL_FALSE;
	}
	return GL_TRUE;
}

Mesh* MeshManager::getAt(GLint position) {
	if (position >= 0 && position < mLoadedMesh.size()) {
		return mLoadedMesh[position];
	}
	return  NULL;
}

GLint MeshManager::loadMesh(Mesh* meshToLoad) {
	mLoadedMesh.push_back(new Mesh(meshToLoad->mVertices, meshToLoad->mIndices));
	return mLoadedMesh.size() - 1;
}

GLvoid MeshManager::drawMesh(GLint meshInt) {
	mLoadedMesh[meshInt]->draw();
}
