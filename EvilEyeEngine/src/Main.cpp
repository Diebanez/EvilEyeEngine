#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Core/RenderingManager.h"
#include "Resources/ResourceManager.h"
#include "Resources/Mesh/Mesh.h"
#include "Resources/Mesh/MeshManager.h"
#include "Resources/Shader/Shader.h"
#include "Resources/Shader/ShaderManager.h"
#include "Resources/Shader/ShaderManager.h"
#include "Resources/Mesh/Model.h"
#include "Core/Transform.h"

GLint meshId;
GLint shaderId;
GLint textureId;

GLfloat actualRotation = 0;
GLfloat lastTime = 0;

Model loadingModel;

Transform testTransform;

GLboolean initEngine();
GLvoid updateTick();
GLboolean disposeEngine();

int main() {
	if (!initEngine()) {
		std::cerr << "Failed Engine Initialization" << std::endl;
		return -1;
	}

	loadingModel = Model("res/models/robot.obj");

	shaderId = ResourceManager::get()->getShaderId("res/shaders/basic.vert", "res/shaders/basic.frag");
	textureId = ResourceManager::get()->getTextureId("res/textures/robot_diffuse.jpg");

	lastTime = glfwGetTime();

	testTransform.translate(glm::vec3(0, -2.5, 7.5));

	while (!glfwWindowShouldClose(RenderingManager::get()->rWindow)) {
		updateTick();
	}

	if (!disposeEngine) {
		std::cerr << "Failed Engine Dispose" << std::endl;
		return -1;
	}
}

GLboolean initEngine() {
	if (!ResourceManager::get()->initialize()) {
		return GL_FALSE;
	}

	if (!RenderingManager::get()->initialize()) {
		return GL_FALSE;
	}

	return GL_TRUE;
}

GLvoid updateTick() {	
	glfwPollEvents();

	GLfloat actualTime = glfwGetTime();

	GLfloat deltaTime = actualTime - lastTime;

	lastTime = actualTime;

	testTransform.rotate(glm::vec3(0, 5 * deltaTime, 0));

	//RenderingManager::get()->pipelinePush(meshId, shaderId);
	for (unsigned int i = 0; i < loadingModel.meshes.size(); i++) {
		RenderingManager::get()->pipelinePush(loadingModel.meshes[i], shaderId, textureId, testTransform.getModelMatrix());
	}

	RenderingManager::get()->renderFrame();
}

GLboolean disposeEngine() {
	if(!ResourceManager::get()->dispose()) {
		return GL_FALSE;
	}

	if (!RenderingManager::get()->dispose()) {
		return GL_FALSE;
	}

	return GL_TRUE;
}