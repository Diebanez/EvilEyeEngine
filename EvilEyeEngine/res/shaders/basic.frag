#version 330 core

in vec2 TexCoord;
out vec4 frag_color;

uniform sampler2D texSampler;

void main()
{
	vec4 colRes = texture(texSampler, TexCoord);
	vec4 tint = vec4(0.5f, 0.5f, 0.5f, 1.0f);
	frag_color = colRes * tint;
}
